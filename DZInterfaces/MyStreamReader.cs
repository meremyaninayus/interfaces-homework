﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace DZInterfaces
{
    public class MyStreamReader<T> : IEnumerable<T>, IDisposable

    {
        #region Private Fields
        private bool _disposed = false;
        private T[] _arr;
        private ISerializer<T[]> _serializer;
        private string _filepath;
        private FileStream _filestream;
        #endregion

        #region Constructors
        public MyStreamReader(string filepath, ISerializer<T[]> serializer)
        {
            _filepath = filepath;
            _serializer = serializer;
        }
        #endregion

        #region Interface implementation
        public void Dispose() => Dispose(true);
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                if (_filestream != null)
                    _filestream.Dispose();
            }
            _disposed = true;
        }

        public IEnumerator<T> GetEnumerator()
        {
            _filestream = new FileStream(_filepath, FileMode.Open);
            using (var reader = new StreamReader(_filestream))
            {
                _arr = _serializer.Deserialize(reader.ReadToEnd());
            }

            foreach (var t in _arr)
            {
                yield return t;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion


    }
}
