﻿using System;

namespace DZInterfaces
{
    public interface ISerializer<T>
    {
        string Serialize(T item);
        T Deserialize(string str);
    }
}
