﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DZInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var myStreamReader = new MyStreamReader<Currency>(@"C:\OTUS-repo\valuta.txt", new MyJsonSerializer<Currency[]>()))
            {
                var currencySorter = new CurrencySorter<Currency>();
                var sortedCurrncy = currencySorter.BubbleSort(myStreamReader.ToArray());
                foreach (var currency in myStreamReader)
                {
                    Console.WriteLine(currency.ToString());
                }
            }

            Console.ReadKey();
        }
    }
}
