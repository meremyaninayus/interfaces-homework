﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZInterfaces
{
    public class Currency:IComparable<Currency>
    {
        public string ID { get; set; }
        public string NumCode { get; set; }
        public string CharCode { get; set; }
        public int Nominal { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public decimal Previous { get; set; }

        public int CompareTo(Currency other)
        {
            if (this.Value == other.Value) return 0;
            if (this.Value > other.Value) return 1;
            return -1;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", Name, Value);
        }
    }
}
