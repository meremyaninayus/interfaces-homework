﻿using System;
using System.IO;
using System.Text.Json;



namespace DZInterfaces
{
    class MyJsonSerializer<T> : ISerializer<T>
    {
        public T Deserialize(String str)
        {
            var currency = JsonSerializer.Deserialize<T>(str);
            return currency;
        }

        public string Serialize(T item)
        {
            var res = JsonSerializer.Serialize<T>(item);
            return res;
        }
    }
}
