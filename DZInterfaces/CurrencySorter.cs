﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZInterfaces
{
    public class CurrencySorter<T> : IAlgoritm<T>
        where T : IComparable<T>
    {
        public T[] BubbleSort(T[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                for (int j = 0; j < arr.Length - 1 - i; j++)
                {
                    if (arr[j].CompareTo(arr[j + 1]) > 0)
                    {
                        T dummy = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = dummy;
                    }
                }
            }
            return arr;
        }
    }
}
