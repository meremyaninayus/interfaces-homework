﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountRepository
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> _repository;

        public AccountService(IRepository<Account> repository)
        {
            this._repository = repository;
        }

        public void AddAccount(Account account)
        {
            if (account.BirthDate.AddYears(18)<DateTime.Now)
            {
                _repository.Add(account);
            }
            else
            {
                throw new ArgumentOutOfRangeException("BirthDate", "Невозможно добавить человека младше 18 лет");
            }
        }
    }
}
