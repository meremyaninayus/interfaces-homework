﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountRepository
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository<Account>(@"C:\OTUS-repo\accounts.txt");
            var accountService = new AccountService(repository);
           accountService.AddAccount(new Account()
            {
                BirthDate = DateTime.Parse("2010-01-01"),
                FirstName = "Маша",
                LastName = "Семенова"
            });
            var accounts = repository.GetAll();
            Func<Account, bool> predicate = (x) =>
            {
                return (x.FirstName == "Маша");
            };
            var account = repository.GetOne(predicate);

        }
    }
}
