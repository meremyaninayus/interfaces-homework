﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AccountRepository
{
    public class Repository<T> : IRepository<T>
    {
        private string _filepath;

        public Repository(string filepath)
        {
            this._filepath = filepath;
        }

        public void Add(T item)
        {
            using (StreamWriter sw = File.AppendText(_filepath))
            {
                var stingToApend = JsonSerializer.Serialize<T>(item);
                sw.WriteLine(stingToApend);
            }
        }

        public IEnumerable<T> GetAll()
        {
            using (StreamReader sr = new StreamReader(new FileStream(_filepath, FileMode.OpenOrCreate)))
            {
                while (!sr.EndOfStream)
                {
                    var jsonString = sr.ReadLine();
                    yield return JsonSerializer.Deserialize<T>(jsonString);
                }
            }
        }

        public T GetOne(Func<T, bool> predicate)
        {
            var items=this.GetAll().ToList();
            return items.FirstOrDefault(predicate);
        }
    }
}
