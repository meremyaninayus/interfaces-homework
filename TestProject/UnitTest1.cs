using Moq;
using NUnit.Framework;
using AccountRepository;
using System.Collections.Generic;
using System;

namespace TestProject
{
    public class Tests
    {
        private Mock<IRepository<Account>> _m;
        private List<Account> _testAccounts = new List<Account>();
        [SetUp]
        public void Setup()
        {
            _m = new Mock<IRepository<Account>>();
            _m.Setup(x => x.GetAll()).Returns(GetAll());
            _m.Setup(a => a.Add(It.IsAny<Account>())).Callback<Account>(Add);
        }
         

        [Test]
        public void TestUnvalidAge()
        {
            var accountServive = new AccountService(_m.Object);
            var account = new Account()
            {
                BirthDate = DateTime.Parse("2010-01-01"),
                FirstName = "����",
                LastName = "��������"
            };
            Assert.Throws<ArgumentOutOfRangeException>(() => accountServive.AddAccount(account));
        }
        
 
        private IEnumerable<Account> GetAll()
        {
            return _testAccounts;
        }
        private void Add(Account item)
        {
            _testAccounts.Add(item);
        }

    }
}